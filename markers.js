export const food = [
	{
		name: "Apple",
		icon: L.icon({
			iconUrl: "./icons/webp/apple.webp",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Berries",
		icon: L.icon({
			iconUrl: "./icons/berries.png",
			iconSize: [ 22, 22 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Brown Mushroom",
		icon: L.icon({
			iconUrl: "./icons/webp/brown-mushroom.webp",
			iconSize: [ 20, 20 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Carrot",
		icon: L.icon({
			iconUrl: "./icons/webp/carrot.webp",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Honey",
		icon: L.icon({
			iconUrl: "./icons/webp/honey.webp",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Lemon",
		icon: L.icon({
			iconUrl: "./icons/webp/lemon.webp",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Lettuce",
		icon: L.icon({
			iconUrl: "./icons/webp/lettuce.webp",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Orange",
		icon: L.icon({
			iconUrl: "./icons/orange.png",
			iconSize: [ 16, 16 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Pumpkin",
		icon: L.icon({
			iconUrl: "./icons/pumpkin.png",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Strawberries",
		icon: L.icon({
			iconUrl: "./icons/strawberry.png",
			iconSize: [ 24, 24 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Truffle",
		icon: L.icon({
			iconUrl: "./icons/truffle.png",
			iconSize: [ 22, 22 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Turnip",
		icon: L.icon({
			iconUrl: "./icons/turnip.png",
			iconSize: [ 28, 28 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Watermelon",
		icon: L.icon({
			iconUrl: "./icons/watermelon.png",
			iconSize: [ 20, 20 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Wheat",
		icon: L.icon({
			iconUrl: "./icons/wheat.png",
			iconSize: [ 28, 28 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "White Mushroom",
		icon: L.icon({
			iconUrl: "./icons/white-mushroom.png",
			iconSize: [ 20, 20 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
]

export const resources = [
	{
		name: "Butterflies",
		icon: L.icon({
			iconUrl: "./icons/butterfly.png",
			iconSize: [ 22, 22 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Eggs",
		icon: L.icon({
			iconUrl: "./icons/eggs.png",
			iconSize: [ 16, 16 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Fish",
		icon: L.icon({
			iconUrl: "./icons/fish.png",
			iconSize: [ 20, 20 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Milk",
		icon: L.icon({
			iconUrl: "./icons/milk.png",
			iconSize: [ 20, 20 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Wool",
		icon: L.icon({
			iconUrl: "./icons/wool.png",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
]

export const chests = [
	{
		name: "Common Chest",
		icon: L.icon({
			iconUrl: "./se-marker.svg",
			iconSize: [ 28, 28 ],
			iconAnchor: [ 14, 28 ],
			popupAnchor: [ 0, -14 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Uncommon Chest",
		icon: L.icon({
			iconUrl: "./se-marker.svg",
			iconSize: [ 28, 28 ],
			iconAnchor: [ 14, 28 ],
			popupAnchor: [ 0, -14 ]
		}),
		markerType: "respawning",
		respawnTime: 7,
	},
	{
		name: "Rare Chest",
		icon: L.icon({
			iconUrl: "./se-marker.svg",
			iconSize: [ 28, 28 ],
			iconAnchor: [ 14, 28 ],
			popupAnchor: [ 0, -14 ]
		}),
		markerType: "respawning",
		respawnTime: 14,
	},
	{
		name: "Epic Chest",
		icon: L.icon({
			iconUrl: "./se-marker.svg",
			iconSize: [ 28, 28 ],
			iconAnchor: [ 14, 28 ],
			popupAnchor: [ 0, -14 ]
		}),
		markerType: "respawning",
		respawnTime: 30,
	},
	{
		name: "Legendary Chest",
		icon: L.icon({
			iconUrl: "./se-marker.svg",
			iconSize: [ 28, 28 ],
			iconAnchor: [ 14, 28 ],
			popupAnchor: [ 0, -14 ]
		}),
		markerType: "one-time",
	},
]

export const collectibles = [
	{
		name: "Sheriff Badge",
		icon: L.icon({
			iconUrl: "./icons/sheriffs-badge.png",
			iconSize: [ 24, 24 ]
		}),
		markerType: "one-time",
	},
	{
		name: "Horseshoe",
		icon: L.icon({
			iconUrl: "./icons/horseshoe.png",
			iconSize: [ 18, 18 ]
		}),
		markerType: "one-time",
	},
	{
		name: "Toy Unicorn",
		icon: L.icon({
			iconUrl: "./icons/toy-unicorn.png",
			iconSize: [ 24, 24 ]
		}),
		markerType: "one-time",
	},
	{
		name: "Bottle",
		icon: L.icon({
			iconUrl: "./icons/message-in-a-bottle.png",
			iconSize: [ 18, 24 ]
		}),
		markerType: "one-time",
	}
]

export const other = [
	{
		name: "Picnic Basket",
		icon: L.icon({
			iconUrl: "./icons/picnic-basket.png",
			iconSize: [ 18, 18 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Wood",
		icon: L.icon({
			iconUrl: "./icons/wood.png",
			iconSize: [ 22, 22 ]
		}),
		markerType: "respawning",
		respawnTime: 1,
	},
	{
		name: "Cave Entrance",
		icon: L.icon({
			iconUrl: './icons/cave-entrance.svg',
			iconSize: [ 28, 28 ],
			iconAnchor: [ 14, 14 ],
		}),
		markerType: "static",
	}
]
