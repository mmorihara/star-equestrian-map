# Star Equestrian Map (WIP)

<img src="assets/StarEquestrianLogo_Color.png" alt="Star Equestrian game logo" width="300" />


An interactive map for [Star Equestrian](https://www.foxieventures.com/star-equestrian/).

Very much a work in progress, but maybe someone will find it useful.

Not too much to say about it yet. Click on the icons to open a popup that might have more information about the item's location. Use the checkboxes in the side menu to show/hide icons.

Also, shoutout to the awesome folks at Foxie Ventures who provided me with a large map image.

Take a look at the dev branch if you want to see the beautiful new basemap.

## To Do

- [x] Finish basemap and patch various defects (thanks, @Random Spider!)
- [ ] Fix weird popup placement and icon sizing
- [ ] Different icons for chests
- [x] Add ability to mark items as collected
- [ ] Finish adding locations
- [x] UI improvements (side menu, popups, etc.)
- [ ] Accessibility
- [ ] Mobile support
- [ ] Marker visibility

---

This is an unofficial fan-made project. Game, logo, and all imagery &copy; [Foxie Ventures](https://www.foxieventures.com).

**Credits:**

- [MapTiler Engine](https://www.maptiler.com/engine/)
- [Leaflet](https://leafletjs.com)
- [Leaflet.contextmenu](https://github.com/aratcliffe/Leaflet.contextmenu)
- [Leaflet.SidePanel](https://github.com/maxwell-ilai/Leaflet.SidePanel)
- [Shoelace](https://shoelace.style)